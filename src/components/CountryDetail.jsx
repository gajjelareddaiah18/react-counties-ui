import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';


const CountryDetails = () => {
  const { cca2 } = useParams();
  const [country, setCountry] = useState(null);

  useEffect(() => {
    const fetchCountryDetails = async () => {
      try {
        const response = await fetch(
          `https://restcountries.com/v3.1/alpha/${cca2}`
        );
        if (!response.ok) {
          throw new Error("Failed to fetch country details");
        }
        const data = await response.json();
        setCountry(data[0]);
        console.log(data);
      } catch (error) {
        console.error("Error fetching country details:", error);
      }
    };

    fetchCountryDetails();
  }, [cca2]);

  if (!country) {
    return <div>Loading...</div>;
  }

  const {
    name,
    flags,
    region,
    capital,
    population,
    subregion,
    currencies,
    languages,
    borders,
    tld,
  } = country;

  return (
    <div className="countire-details">
      <Link to="/" className="button2">
      <FontAwesomeIcon icon={faArrowLeft} />   <span className="back-text">Back</span>
      </Link>
      <div className="selected">
        <div className="box1">
          <img className="imgs-1" src={flags.png} alt={name.common} />
          <div className="inside-disply">
            <div className="inside-1">
              <div className="insides1">
                <h2 className="information1">{name.common}</h2>
                <p className="information1">
                  <strong>Native Name</strong>:{" "}
                  {name.nativeName?.tur?.common || name.nativeName?.eng?.common ||name.nativeName?.por?.common||name.nativeName?.ita?.common||name.nativeName?.tuk?.common||name.nativeName?.kal?.common|| "Not available"}
                </p>
                <p className="information1">
                  <strong>Population</strong>: {population}
                </p>
                <p className="information1">
                  <strong>Region</strong>: {region}
                </p>
                <p className="information1">
                  <strong>Subregion</strong>: {subregion}
                </p>
                <p className="information1">
                  <strong>Capital</strong>: {capital}
                </p>
              </div>
              <div className="inside2">
              <p className="information1">
               <strong>Top Level Domain:</strong>
              {tld}
                  </p>
                <p className="information1">
                  <strong>Currencies</strong>:{" "}
                  {Object.keys(currencies || {}).join(", ")}
                </p>
                <p className="information1">
                  <strong>Languages:</strong>{Object.values(languages).join(" ,")}
                </p>
              </div>
            </div>
            <p className=" information2">
              <strong>Country Borders:</strong>:
              {borders && borders.length > 0 ? (
                borders.map((border, index) => (
                  <button className="button1" key={index}>{border}</button>
                ))
              ) : (
                <span>No borders</span>
              )}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CountryDetails;

import * as React from 'react';
import ToggleButton from './togglebutton';
import '../App.css';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Brightness3Icon from '@mui/icons-material/Brightness3';

export default function Header() {
  return (

     <Box sx={{ flexGrow: 1 }}>
      <AppBar className='display1  heder ' position="static" sx={{ height: 100, justifyContent: "center", backgroundColor: "rgb(255,255,255)", color: "black"}}>
        <Toolbar sx={{width:"80%",marginLeft:"auto",marginRight:"auto"}}>
          <Typography variant="h4" component="div" sx={{ flexGrow: 1 }}>
            Where in the World?
          </Typography>
          <Brightness3Icon sx={{ height: '1rem', mr: 2 }} />
          <ToggleButton sx={{ border: "none" }}></ToggleButton>       
        </Toolbar>
      </AppBar>
    </Box>

  );
}

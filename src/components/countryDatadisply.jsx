import React from "react";
import { Link } from "react-router-dom";
import { Grid, Typography } from "@mui/material";
import { ImageListItem, ImageListItemBar } from "@mui/material";
import './style.css';

export default function CountryDataDisplay({ filterCountries }) {
  return (
    <Grid className="display" container spacing={5} sx={{width:"80%",mt:2,marginLeft:20}}>
      {filterCountries.map((country) => (
        <Grid item key={country.cca2} xs={12} sm={6} md={3} lg={3}  sx={{width:"80%",marginLeft:"auto",marginRight:"auto" }}>
          <Link to={`/country/${country.cca2}`} style={{ textDecoration: 'none' }}>
            <ImageListItem className="boxs" sx={{ borderRadius: 1 ,boxShadow: "rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px" ,backgroundColor:"rgb(255,255,255)"}}>
              <img
                src={country.flags.png}
                alt={country.name.common}
                loading="lazy"
                style={{ width: "100%", height: "200px", objectFit: "cover" }}
              />
              <ImageListItemBar 
                title={<Typography className="title" >{country.name.common}</Typography>}
                subtitle={ 
                  <React.Fragment >
                    <Typography className="displaytext" variant="subtitle1"><strong>Population:</strong> {country.population}</Typography>
                    <Typography   className="displaytext"  variant="subtitle1"><strong>Region:</strong> {country.region}</Typography>
                    <Typography className="displaytext"  variant="subtitle1"><strong>Capital:</strong> {country.capital}</Typography>
                  </React.Fragment>
                }
                position="below"
                sx={{
                  "& .MuiImageListItemBar-title": {
                    fontSize: "24px",
                    fontWeight: "bold",
                    color: "black",
                    paddingBottom: "10px"
                  },
                  "& .MuiImageListItemBar-subtitle": {
                    fontSize: "16px",
                    color: "black",
                    lineHeight: 1.5,
                  },
                  padding: "20px",
                }}
              />
            </ImageListItem>
          </Link>
        </Grid>
      ))}
    </Grid>
  );
}

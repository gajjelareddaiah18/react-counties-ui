
import React, { useState } from 'react';

export default function () {
    const [isDarkMode, setIsDarkMode] = useState(false);

    const toggleDarkMode = () => {
        setIsDarkMode(!isDarkMode);
        document.body.classList.toggle('dark-mode');
        document.body.background
    };

  return (
    <div>
 <button className={`toggle-button ${isDarkMode ? 'dark' : 'light'}`} onClick={toggleDarkMode}>Dark Mode</button>


    </div>
  );
}

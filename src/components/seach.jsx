import React, { useState } from 'react';
import '../App.css'; 
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem'; 
import InputBase from '@mui/material/InputBase'; 
import SearchIcon from '@mui/icons-material/Search';
import { styled, alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import { InputLabel } from '@mui/material';
const SearchBox = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
 
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
   
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  width: '100%',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

export default function Search({ regions, subregions, handleRegionChange, handleCountryChange, handleSubRegionChange, handleSortChange, handleSortAreaChange, searchTerm }) {
  const [isRegionSelected, setIsRegionSelected] = useState(false); 
  const [sortOption, setSortOption] = useState(""); // State to store sort option
  const [sortAreaOption, setSortAreaOption] = useState("");

  const handleChange = (e) => {
    const selectedRegion = e.target.value;
    handleRegionChange(selectedRegion);
    setIsRegionSelected(!!selectedRegion);
  };

  const handleSearch = () => {
    handleCountryChange(searchTerm);
  };

  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      handleSearch();
    }
  };

  const handleSort = (e) => {
    const selectedSortOption = e.target.value;
    setSortOption(selectedSortOption);
    handleSortChange(selectedSortOption);
  };

  const handleAreaSort = (event) => {
    const selectedSortOption = event.target.value;
    setSortAreaOption(selectedSortOption);
    handleSortAreaChange(selectedSortOption); 
  };

  return (
    <Toolbar className='display' position="static" sx={{  paddingTop:"70px",width:"80%",marginLeft:"auto",marginRight:"auto"}}>

      <Box className="selectbut" sx={{ flexGrow: 1, borderRadius: 1, marginRight: 40, boxShadow: "rgba(0, 0, 0, 0.1) 0px 4px 12px" ,background:"rgb(255,255,255)",border:"none"}}>
        <SearchBox>
          <SearchIconWrapper>
            <SearchIcon />
          </SearchIconWrapper>
          <StyledInputBase
            value={searchTerm}
            onChange={(e) => handleCountryChange(e.target.value)} 
            onKeyDown={handleKeyDown}
            placeholder="Search…"
            inputProps={{ 'aria-label': 'search' }}
          />
        </SearchBox>
      </Box>
      
      {regions.length > 0 && (
        <FormControl className="selectbut" sx={{ width: "180px", mr: 1,boxShadow: "rgba(0, 0, 0, 0.1) 0px 4px 12px",background:"rgb(255,255,255)" }}>
          <InputLabel  id="demo-mutiple-name-label">Filter by Region</InputLabel>
          <Select label="Filter by Region" onChange={handleChange}>
            <MenuItem  value="">Filter by Region</MenuItem>
            {regions.map((region, index) => (
              <MenuItem key={region} value={region}>
                {region}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      )}

      {isRegionSelected && (
        <FormControl className="selectbut" sx={{ width: "180px", mr: 1,boxShadow: "rgba(0, 0, 0, 0.1) 0px 4px 12px",background:"rgb(255,255,255)" }}>
          <InputLabel  id="demo-mutiple-name-label">Filter by Subregion</InputLabel>
          <Select label="Filter by Subregion" className="subregions" onChange={(e) => handleSubRegionChange(e.target.value)}>
            <MenuItem  value="">Filter by Subregion</MenuItem>
            {subregions.map((subregion, index) => (
              <MenuItem key={subregion} value={subregion}>
                {subregion}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      )}

      <FormControl className="selectbut" sx={{ width: "180px", mr: 1 ,boxShadow: "rgba(0, 0, 0, 0.1) 0px 4px 12px",background:"rgb(255,255,255)"}}>
      <InputLabel  id="demo-mutiple-name-label">Sort by Population</InputLabel>
        <Select label="Sort by population" value={sortOption} onChange={handleSort}>
          <MenuItem value="">Sort by Population</MenuItem>
          <MenuItem value="asc"> Low to High</MenuItem>
          <MenuItem value="desc">High to Low</MenuItem>
        </Select>
      </FormControl>

      <FormControl className="selectbut" sx={{ width: "150px" ,boxShadow: "rgba(0, 0, 0, 0.1) 0px 4px 12px",background:"rgb(255,255,255)"}}>
      <InputLabel id="demo-mutiple-name-label">Sort by Area</InputLabel>
      <Select  label="Sort by Area"   value={sortAreaOption} onChange={handleAreaSort} >
          <MenuItem value="">Sort by Area</MenuItem>
          <MenuItem value="asc">Low to High</MenuItem>
          <MenuItem value="desc"> High to Low</MenuItem>
        </Select>
      </FormControl>
    </Toolbar>
  );
}

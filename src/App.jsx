
import './App.css'
import Header from './components/Header'
import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CountriesData from "./components/countiesdata";
import CountryDetails from "./components/CountryDetail"; 


function App() {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch("https://restcountries.com/v3.1/all");
      if (!response.ok) {
        throw new Error("Failed to fetch data");
      }
      const jsonData = await response.json();
      setData(jsonData);
    } catch (error) {
      setError(error.message);
    }
  }

  
  return (
    <>
     <BrowserRouter>
      <div>
        <Header></Header>
        <Routes>
          <Route exact path="/" element={<CountriesData data={data} error={error} />} />
          <Route path="/country/:cca2" element={<CountryDetails />} />
        </Routes>
      </div>
    </BrowserRouter>

     
    </>
  )
}

export default App
